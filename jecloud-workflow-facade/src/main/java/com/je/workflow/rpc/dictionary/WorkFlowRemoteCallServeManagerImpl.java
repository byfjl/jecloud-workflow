/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.rpc.dictionary;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.apache.servicecomb.provider.pojo.RpcSchema;

import java.util.HashMap;
import java.util.Map;

@RpcSchema(schemaId = "workFlowRemoteCallServeManager")
public class WorkFlowRemoteCallServeManagerImpl implements WorkFlowRemoteCallServeManager {

    @Override
    public Object doGetDynaBean(String id, String tableCode) {
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        try {
            return metaService.selectOneByPk(tableCode, id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Object doUpdateBean(Object bean, String id, String tableCode) {
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        try {
            Map<String, Object> values = (Map<String, Object>) bean;
            DynaBean dynaBean = new DynaBean(tableCode, true);
            String pkCode = dynaBean.getPkCode();
            values.put(pkCode, id);
            values.put(BeanService.KEY_TABLE_CODE, tableCode);
            dynaBean.setValues((HashMap<String, Object>) values);
            return metaService.update(dynaBean);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deployClearBusinessDataWorkFlowFiledInfo(String tableCode, String modelKey) {
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        if (Strings.isNullOrEmpty(modelKey)) {
            return;
        }
        DynaBean dynaBean = new DynaBean();
        dynaBean.setStr("SY_STARTEDUSER", "");
        dynaBean.setStr("SY_STARTEDUSERNAME", "");
        dynaBean.setStr("SY_APPROVEDUSERS", "");
        dynaBean.setStr("SY_APPROVEDUSERNAMES", "");
        dynaBean.setStr("SY_PREAPPROVUSERS", "");
        dynaBean.setStr("SY_LASTFLOWINFO", "");
        dynaBean.setStr("SY_PREAPPROVUSERNAMES", "");
        dynaBean.setStr("SY_LASTFLOWUSER", "");
        dynaBean.setStr("SY_LASTFLOWUSERID", "");
        dynaBean.setStr("SY_WFWARN", "");
        dynaBean.setStr("SY_PIID", "");
        dynaBean.setStr("SY_PDID", "");
        dynaBean.setStr("SY_WARNFLAG", "");
        dynaBean.setStr("SY_CURRENTTASK", "");
        dynaBean.setStr("SY_AUDFLAG", "NOSTATUS");
        metaService.update(dynaBean, ConditionsWrapper.builder().table(tableCode).like("SY_PDID", modelKey));
    }

}
