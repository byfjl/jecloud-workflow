/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

import java.util.List;
import java.util.Map;

public class EventSubmitDTO {
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 任务名称
     */
    private String currentTaskName;
    /**
     * 目标任务
     */
    private String targetTaskName;
    /**
     * 目标路线
     */
    private String targetTransition;
    /**
     * 提交类型（通过或退回）
     */
    private OperatorEnum operatorEnum;
    /**
     * 提交意见
     */
    private String submitComment;
    /**
     * 处理人信息
     */
    private List<Map<String, String>> assignees;
    /**
     * 功能编码
     */
    private String funcCode;
    /**
     * 执行Bean
     */
    private Map<String, Object> dynaBean;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCurrentTaskName() {
        return currentTaskName;
    }

    public void setCurrentTaskName(String currentTaskName) {
        this.currentTaskName = currentTaskName;
    }

    public String getTargetTaskName() {
        return targetTaskName;
    }

    public void setTargetTaskName(String targetTaskName) {
        this.targetTaskName = targetTaskName;
    }

    public String getTargetTransition() {
        return targetTransition;
    }

    public void setTargetTransition(String targetTransition) {
        this.targetTransition = targetTransition;
    }

    public OperatorEnum getOperatorEnum() {
        return operatorEnum;
    }

    public void setOperatorEnum(OperatorEnum operatorEnum) {
        this.operatorEnum = operatorEnum;
    }

    public String getSubmitComment() {
        return submitComment;
    }

    public void setSubmitComment(String submitComment) {
        this.submitComment = submitComment;
    }

    public List<Map<String, String>> getAssignees() {
        return assignees;
    }

    public void setAssignees(List<Map<String, String>> assignees) {
        this.assignees = assignees;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public Map<String, Object> getDynaBean() {
        return dynaBean;
    }

    public void setDynaBean(Map<String, Object> dynaBean) {
        this.dynaBean = dynaBean;
    }
}
