/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.controller.procerss;

import com.google.common.collect.Lists;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.rbac.annotation.ControllerAuditLog;
import com.je.workflow.service.usertask.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 流程统计
 */
@RestController
@RequestMapping(value = "/je/workflow/statistics")
public class StatisticsController extends AbstractPlatformController {

    @Autowired
    private MonitorService monitorService;

    @Override
    @RequestMapping(
            value = {"/load"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "重写流程监控load方法"
    )
    @ControllerAuditLog(moduleName = "流程监控模块",operateTypeCode = "loadWorkflow",operateTypeName = "查看流程监控",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        Page page = monitorService.load(param, request);
        return page == null ? BaseRespResult.successResultPage(Lists.newArrayList(), 0L)
                : BaseRespResult.successResultPage(page.getRecords(), (long) page.getTotal());
    }

    @RequestMapping(
            value = {"/clearDirtyData"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "清理流程待办脏数据"
    )
    @ControllerAuditLog(moduleName = "流程监控模块",operateTypeCode = "clearWorkflowDirty",operateTypeName = "清理流程待办脏数据",logTypeCode = "systemManage",logTypeName = "系统管理")
    public void clearDirtyData() {
        monitorService.clearDirtyData();
    }


}
