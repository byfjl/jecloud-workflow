/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import com.je.bpm.core.model.config.ProcessRemindTypeEnum;
import com.je.bpm.core.model.config.process.ProcessRemindTemplate;
import com.je.workflow.service.push.pojo.MessageDTO;

import java.util.List;
import java.util.Map;

public interface PushService {

    /**
     * 发送消息
     *
     * @param userId
     * @param funcCode
     * @param modelName
     * @param submitType
     * @param comment
     * @param pkValue
     */
    public void pushMsg(String userId, String funcCode, String modelName, String submitType, String comment, String pkValue);

    /**
     * 刷新列表
     *
     * @param userId
     */
    public void pushRefresh(String userId);

    /**
     * 发送不同类型消息
     *
     * @param userId
     * @param processRemindTypeEnumList
     * @param remindTemplate
     * @param messageDTO
     */
    public void pushDiffTypeMsg(String userId, List<ProcessRemindTypeEnum> processRemindTypeEnumList, List<ProcessRemindTemplate> remindTemplate, MessageDTO messageDTO);

    public void pushUrgeMsg(String userId, MessageDTO messageDTO, List<String> list, String customerContent, String reminderName);

    public void pushUrgeMsg(String userId, MessageDTO messageDTO, List<String> list, String customerContent, String reminderName,
                            Map<String, String> context);

    /**
     * 发送审批告知消息
     *
     * @param userId
     * @param funcCode
     * @param modelName
     * @param comment
     * @param pkValue
     */
    public void pushApprovalNoticeMsg(String userId, String funcCode, String modelName, String title, String comment, String pkValue);
}
