/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import com.je.bpm.core.model.config.ProcessRemindTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PushMessageFactory {

    private static AbstractPushMessageService webMessage;
    private static AbstractPushMessageService emailMessage;
    private static AbstractPushMessageService noteMessage;
//    private static AbstractPushMessageService wechatMessage;
    private static AbstractPushMessageService dingtalkMessage;
//    private static AbstractPushMessageService flybookMessage;
//    private static AbstractPushMessageService welinkMessage;
    private static AbstractPushMessageService customMessage;
    private static AbstractPushMessageService allMessage;

    @Autowired
    public PushMessageFactory(AbstractPushMessageService webMessage,
                              AbstractPushMessageService emailMessage,
                              AbstractPushMessageService noteMessage,
//                              AbstractPushMessageService wechatMessage,
                              AbstractPushMessageService dingtalkMessage,
//                              AbstractPushMessageService flybookMessage,
//                              AbstractPushMessageService welinkMessage,
                              AbstractPushMessageService customMessage) {
        PushMessageFactory.webMessage = webMessage;
        PushMessageFactory.emailMessage = emailMessage;
        PushMessageFactory.noteMessage = noteMessage;
//        PushMessageFactory.wechatMessage = wechatMessage;
        PushMessageFactory.dingtalkMessage = dingtalkMessage;
//        PushMessageFactory.flybookMessage = flybookMessage;
//        PushMessageFactory.welinkMessage = welinkMessage;
        PushMessageFactory.customMessage = customMessage;
        PushMessageFactory.allMessage = webMessage;
    }

    public static AbstractPushMessageService createMessage(ProcessRemindTypeEnum messageType) {
        switch (messageType) {
            case WEB:
                return webMessage;
            case EMAIL:
                return emailMessage;
            case NOTE:
                return noteMessage;
//            case WECHAT:
//                return wechatMessage;
            case DINGTALK:
                return dingtalkMessage;
//            case FLYBOOK:
//                return flybookMessage;
//            case WELINK:
//                return welinkMessage;
            case CUSTOM:
                return customMessage;
            case ALL:
                return allMessage;
            default:
                throw new IllegalArgumentException("Unsupported message type: " + messageType);
        }
    }
}



