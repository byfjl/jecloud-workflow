/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 作废
 */
@Service
public class InvalidUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {
        endExecution(beanId, SubmitTypeEnum.INVALID, comment);
    }

    private void endExecution(String beanId, SubmitTypeEnum submitType, String comment) {
        List<DynaBean> executions = metaService.select("JE_WORKFLOW_RN_EXECUTION",
                ConditionsWrapper.builder().eq("BUSINESS_KEY", beanId));
        for (DynaBean execution : executions) {
            DynaBean hiExecution = buildHiExecution(execution, submitType, comment);
            List<DynaBean> taskList = metaService.select("JE_WORKFLOW_RN_TASK", ConditionsWrapper.builder().
                    eq("JE_WORKFLOW_RN_EXECUTION_ID", execution.getPkValue()));
            addHiTask(taskList, hiExecution);
        }
        metaService.delete("JE_WORKFLOW_RN_EXECUTION", ConditionsWrapper.builder().eq("BUSINESS_KEY", beanId));
    }

    private void addHiTask(List<DynaBean> tasks, DynaBean hiExecution) {
        for (DynaBean task : tasks) {
            DynaBean hiTask = new DynaBean("JE_WORKFLOW_HI_TASK", true);
            hiTask.setStr("JE_WORKFLOW_HI_EXECUTION_ID", hiExecution.getStr("JE_WORKFLOW_HI_EXECUTION_ID"));
            hiTask.setStr("ASSIGNEE_NAME", task.getStr("ASSIGNEE_NAME"));
            hiTask.setStr("ASSIGNEE_ID", task.getStr("ASSIGNEE_ID"));
            hiTask.setStr("TASK_COLLECT_TIME", task.getStr("TASK_COLLECT_TIME"));
            hiTask.setStr("TASK_COLLECT", task.getStr("TASK_COLLECT"));
            hiTask.setStr("TASK_SUBMIT_USER_ID", task.getStr("TASK_SUBMIT_USER_ID"));
            hiTask.setStr("TASK_SUBMIT_USER", task.getStr("TASK_SUBMIT_USER"));
            hiTask.setStr("TASK_COMMENT", task.getStr("TASK_COMMENT"));
            hiTask.setStr("TASK_SUBMIT_TYPE", task.getStr("TASK_SUBMIT_TYPE"));
            hiTask.setStr("CURRENT_ASSIGNEE", task.getStr("CURRENT_ASSIGNEE"));
            hiTask.setStr("CURRENT_ASSIGNEE_ID", task.getStr("CURRENT_ASSIGNEE_ID"));
            hiTask.setStr("SY_CREATETIME", task.getStr("SY_CREATETIME"));
            hiTask.setStr("TASK_SUBMIT_TIME", task.getStr("TASK_SUBMIT_TIME"));
            hiTask.setStr("TASK_PDID", task.getStr("TASK_PDID"));
            hiTask.setStr("TASK_PIID", task.getStr("TASK_PIID"));
            metaService.insert(hiTask);
            metaService.delete("JE_WORKFLOW_RN_TASK", ConditionsWrapper.builder()
                    .eq("JE_WORKFLOW_RN_TASK_ID", task.getPkValue()));
        }
    }

}
