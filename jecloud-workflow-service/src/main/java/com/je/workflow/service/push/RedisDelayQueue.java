/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import com.alibaba.fastjson2.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import java.time.LocalDateTime;
import java.util.UUID;

public class RedisDelayQueue<T> {

    Logger log = LoggerFactory.getLogger(RedisDelayQueue.class);

    /**
     * 延迟队列名称
     */
    private String delayQueueName = "delayQueue";
    private RedisTemplate redisTemplate;

    /**
     * 传入redis客户端操作
     */
    public RedisDelayQueue(RedisTemplate redisTemplate, String delayQueueName) {
        this.redisTemplate = redisTemplate;
        this.delayQueueName = delayQueueName;
    }

    /**
     * 设置延迟消息
     */
    public void setDelayQueue(T msg, String msgType, String identification, long delayTime) {
        DelayTask<T> delayTask = new DelayTask<>();
        delayTask.setId(UUID.randomUUID().toString());
        delayTask.setMsg(msg);
        delayTask.setMsgType(msgType);
        delayTask.setIdentification(identification);
        redisTemplate.opsForZSet().add(delayQueueName, JSONObject.toJSONString(delayTask), delayTime);
        log.info("添加任务成功！delayTask={},当前时间为time={}", JSONObject.toJSONString(delayTask), LocalDateTime.now());
    }

}
