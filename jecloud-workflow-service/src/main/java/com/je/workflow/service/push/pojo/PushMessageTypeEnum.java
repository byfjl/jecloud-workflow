/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push.pojo;

public enum PushMessageTypeEnum {
    /**
     * 通知-打开功能表单
     */
    SOCKET_PUSH_OPEN_FUNC_FORM_MSG("socketPush_OpenFuncFormMsg"),
    /**
     * 通知-系统通知
     */
    SOCKET_PUSH_SYSTEM_MESSAGE("socketPush_SystemMessage"),
    /**
     * 短信-带记录
     */
    NOTE_PUSH_WITH_RECORD("notePush_withRecord"),
    /**
     * 邮件
     */
    EMAIL_PUSH_SEND("emailPush_send"),
    /**
     * 钉钉
     */
    DING_TALK_PUSH_SEND("dingTalkPush_send");

    public static PushMessageTypeEnum getPushMessageTypeEnum(String name) {
        if (name.equals(SOCKET_PUSH_OPEN_FUNC_FORM_MSG.name())) {
            return SOCKET_PUSH_OPEN_FUNC_FORM_MSG;
        } else if (name.equals(SOCKET_PUSH_SYSTEM_MESSAGE)) {
            return SOCKET_PUSH_SYSTEM_MESSAGE;
        } else if (name.equals(NOTE_PUSH_WITH_RECORD.name)) {
            return NOTE_PUSH_WITH_RECORD;
        } else if (name.equals(EMAIL_PUSH_SEND)) {
            return EMAIL_PUSH_SEND;
        } else if (name.equals(DING_TALK_PUSH_SEND)) {
            return DING_TALK_PUSH_SEND;
        }
        return null;
    }

    private String name;

    PushMessageTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
