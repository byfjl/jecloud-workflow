/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user.userParser;

import com.je.bpm.core.model.config.task.assignment.AssigneeResource;
import com.je.bpm.core.model.config.task.assignment.BasicAssignmentConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.runtime.shared.identity.UserDepartmentManager;
import com.je.common.base.spring.SpringContextHolder;
import com.je.core.entity.extjs.JSONTreeNode;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 部门
 */
public class UserDepartmentParser extends AbstractUserParser {

    @Override
    public JSONTreeNode parser(String userId,
                               BasicAssignmentConfigImpl basicAssignmentConfig, KaiteBaseUserTask kaiteBaseUserTask,
                               Boolean showCompany,
                               Map<String, Object> bean, String prod, Boolean multiple,Boolean addOwn,Map<String, String> customerMap) {
        UserDepartmentManager userDepartmentManager = SpringContextHolder.getBean(UserDepartmentManager.class);
        buildAssignmentPermissionBo(basicAssignmentConfig.getPermission(), bean);
        List<AssigneeResource> assigneeResourceList = basicAssignmentConfig.getResource();
        String deptIds = assigneeResourceList.stream().map(assigneeResource -> assigneeResource.getId())
                .collect(Collectors.joining(","));
        Object child = userDepartmentManager.findUserDepts(userId, deptIds, assignmentPermissionBo, showCompany, multiple, addOwn);
        if (child != null && child instanceof JSONTreeNode) {
            return (JSONTreeNode) child;
        }
        return null;
    }

    @Override
    public Boolean analysis(String userId, BasicAssignmentConfigImpl basicAssignmentConfig, KaiteBaseUserTask kaiteBaseUserTask,
                            Map<String, Object> bean, String prod, Map<String, String> customerMap) {
        UserDepartmentManager userDepartmentManager = SpringContextHolder.getBean(UserDepartmentManager.class);
        buildAssignmentPermissionBo(basicAssignmentConfig.getPermission(), bean);
        List<AssigneeResource> assigneeResourceList = basicAssignmentConfig.getResource();
        String deptIds = assigneeResourceList.stream().map(assigneeResource -> assigneeResource.getId())
                .collect(Collectors.joining(","));
        return userDepartmentManager.checkContainsCurrentUser(userId, deptIds, assignmentPermissionBo);
    }

}
