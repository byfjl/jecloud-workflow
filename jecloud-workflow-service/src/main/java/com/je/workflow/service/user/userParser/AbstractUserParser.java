/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user.userParser;

import com.google.common.base.Strings;
import com.je.bpm.core.model.config.task.assignment.AssignmentPermission;
import com.je.bpm.core.model.config.task.assignment.BasicAssignmentConfigImpl;
import com.je.bpm.runtime.shared.identity.BO.AssignmentPermissionBo;
import com.je.common.base.util.TreeUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.workflow.service.push.CommonSystemVariable;
import org.springframework.beans.BeanUtils;

import java.util.Map;

public abstract class AbstractUserParser implements UserParser {

    protected AssignmentPermissionBo assignmentPermissionBo;

    protected JSONTreeNode buildRootTree(BasicAssignmentConfigImpl basicAssignmentConfig) {
        JSONTreeNode jsonTreeNode = TreeUtil.buildRootNode();
        jsonTreeNode.setText(basicAssignmentConfig.getConfigTypeName());
        jsonTreeNode.setCode(basicAssignmentConfig.getConfigType());
        return jsonTreeNode;
    }

    protected void buildAssignmentPermissionBo(AssignmentPermission assignmentPermission, Map<String, Object> bean) {
        assignmentPermissionBo = null;
        String formatSql = "";
        if (assignmentPermission != null) {
            String sql = assignmentPermission.getSql();
            //替换变量
            if (!Strings.isNullOrEmpty(sql)) {
                if (bean != null) {
                    for (Map.Entry<String, Object> entry : bean.entrySet()) {
                        if (sql.indexOf(entry.getKey()) != -1) {
                            if (entry.getValue() != null) {
                                sql = sql.replaceAll("\\{" + entry.getKey() + "\\}", String.valueOf(entry.getValue()));
                            } else {
                                sql = sql.replaceAll("\\{" + entry.getKey() + "\\}", "");
                            }
                        }
                    }
                }
                formatSql = CommonSystemVariable.formatVariable(sql);
            }
            assignmentPermissionBo = new AssignmentPermissionBo();
            BeanUtils.copyProperties(assignmentPermission, assignmentPermissionBo);
            assignmentPermissionBo.setSql(formatSql);
        }
    }
}
