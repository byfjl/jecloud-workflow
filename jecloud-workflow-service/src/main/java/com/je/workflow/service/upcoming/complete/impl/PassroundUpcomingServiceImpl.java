/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 传阅
 */
@Service
public class PassroundUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId,
                         Map<String, String> params, Map<String, Object> bean) {
        //新增
        String userIds = params.get("userIds");
        List<String> ids = Arrays.asList(userIds.split(","));
        List<DynaBean> list = metaService.select("JE_WORKFLOW_RN_TASK",
                ConditionsWrapper.builder().eq("TASK_ACTIVITI_TASK_ID", taskId));
        if (list.size() > 0) {
            for (String userId : ids) {
                DynaBean dynaBean = list.get(0);
                dynaBean.setStr("JE_WORKFLOW_RN_TASK_ID", UUID.randomUUID().toString());
                dynaBean.setStr("TASK_CIRCULATION", "1");
                dynaBean.setStr("TASK_HANDLE", "0");
                dynaBean.setStr("ASSIGNEE_ID", userId);
                dynaBean.setStr("ASSIGNEE_NAME", workFlowUserService.getUserNameById(userId));
                metaService.insert(dynaBean);
                String submitUserId = workFlowUserService.getAssociationIdById(Authentication.getAuthenticatedUser().getDeptId());
                String submitUserName = workFlowUserService.getUserNameById(Authentication.getAuthenticatedUser().getDeptId());
                DynaBean execution = metaService.selectOneByPk("JE_WORKFLOW_RN_EXECUTION", dynaBean.getStr("JE_WORKFLOW_RN_EXECUTION_ID"));
                pushMag(dynaBean.getStr("TASK_PDID"), dynaBean.getStr("TASK_PIID"), dynaBean.getStr("TASK_NODE_ID"), userId,
                        SubmitTypeEnum.PASSROUND.getName(), comment, execution.getStr("BUSINESS_KEY")
                        , execution.getStr("FUNC_CODE"), execution.getStr("EXECUTION_NODE_NAME")
                        , submitUserName, submitUserId, bean);
            }
        }

    }

}
