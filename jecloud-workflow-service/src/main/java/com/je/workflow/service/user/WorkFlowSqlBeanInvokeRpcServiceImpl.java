/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.common.base.service.MetaService;
import com.je.common.base.util.TreeUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.rpc.MetaRbacRpcServiceImpl;
import com.je.servicecomb.RpcSchemaFactory;
import com.je.workflow.rpc.dictionary.WorkFlowSqlInvokeRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service(value = "workFlowSqlBeanInvokeRpcService")
public class WorkFlowSqlBeanInvokeRpcServiceImpl implements WorkFlowSqlBeanInvokeRpcService {

    @Autowired
    MetaService metaService;
    @Autowired
    MetaRbacRpcServiceImpl metaRbacRpcService;

    @Override
    public JSONTreeNode invoke(String sql, String prod, Map<String, Object> bean, Boolean multiple, Boolean addOwn) {
        if (prod.equals("rbac")) {
            return invokeRbacSql(sql, bean);
        } else {
            WorkFlowSqlInvokeRpcService workFlowSqlInvokeRpcService =
                    RpcSchemaFactory.getRemoteProvierClazz(prod, "workFlowSqlInvokeRpcService",
                            WorkFlowSqlInvokeRpcService.class);
            List<Map<String, Object>> list = workFlowSqlInvokeRpcService.invoke(sql);
            List<String> accountId = new ArrayList<>();
            for (Map<String, Object> map : list) {
                for (String key : map.keySet()) {
                    if (map.get(key) != null) {
                        accountId.add(String.valueOf(map.get(key)));
                        continue;
                    }
                    continue;
                }
            }
            if (accountId.size() > 0) {
                String sb = accountId.stream().map(id -> id).collect(Collectors.joining(","));
                sql = String.format("SELECT * FROM JE_RBAC_ACCOUNT WHERE JE_RBAC_ACCOUNT_ID IN('%s')", sb);
                return invokeRbacSql(sql, bean);
            } else {
                JSONTreeNode jsonTreeNode = TreeUtil.buildRootNode();
                return jsonTreeNode;
            }
        }
    }

    public JSONTreeNode invokeRbacSql(String sql, Map<String, Object> bean) {
        List<Map<String, Object>> list = metaRbacRpcService.selectMap(sql);
        JSONTreeNode jsonTreeNode = TreeUtil.buildRootNode();
        for (Map<String, Object> map : list) {
            JSONTreeNode node = new JSONTreeNode();
            node.setId(String.valueOf(map.get("JE_RBAC_ACCOUNT_ID")));
            node.setCode(String.valueOf(map.get("ACCOUNT_CODE")));
            node.setText(String.valueOf(map.get("ACCOUNT_NAME")));
            node.setNodeType("LEAF");
            node.setNodeInfo(String.valueOf(map.get("ACCOUNT_CODE")));
            node.setNodeInfoType("user");
            node.setIcon("fal fa-user");
            node.setParent(jsonTreeNode.getId());
            node.setChecked(false);
            node.setLayer(jsonTreeNode.getLayer() + 1);
            node.setBean(map);
            jsonTreeNode.getChildren().add(node);
        }
        return jsonTreeNode;
    }

}
