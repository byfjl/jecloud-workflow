/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.DynaBean;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 转办
 */

@Service
public class TransferUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {
        String transferUserId = params.get("transferUserId");
        DynaBean dynaBean = finishTask(taskId);
        insertTask(dynaBean, transferUserId, SubmitTypeEnum.TRANSFER, comment);

        DynaBean execution = metaService.selectOneByPk("JE_WORKFLOW_RN_EXECUTION", dynaBean.getStr("JE_WORKFLOW_RN_EXECUTION_ID"));
        String submitUserId = workFlowUserService.getAssociationIdById(Authentication.getAuthenticatedUser().getDeptId());
        String submitUserName = workFlowUserService.getUserNameById(Authentication.getAuthenticatedUser().getDeptId());
        pushMag(dynaBean.getStr("TASK_PDID"), dynaBean.getStr("TASK_PIID"), dynaBean.getStr("TASK_NODE_ID"), transferUserId,
                SubmitTypeEnum.TRANSFER.getName(), comment, execution.getStr("BUSINESS_KEY")
                , execution.getStr("FUNC_CODE"), execution.getStr("EXECUTION_NODE_NAME")
                , submitUserName, submitUserId, bean);

    }

}
