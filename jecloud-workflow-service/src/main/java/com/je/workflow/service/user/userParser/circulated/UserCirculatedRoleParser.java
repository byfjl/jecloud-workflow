/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user.userParser.circulated;

import com.je.bpm.core.model.config.task.PassRoundResource;
import com.je.bpm.runtime.shared.identity.BO.PassParserUserBo;
import com.je.bpm.runtime.shared.identity.UserRoleManager;
import com.je.common.base.spring.SpringContextHolder;
import com.je.core.entity.extjs.JSONTreeNode;

/**
 * 角色
 */
public class UserCirculatedRoleParser extends AbstractUserCirculatedParser {

    @Override
    public JSONTreeNode parser(PassParserUserBo passConfig, PassRoundResource passRoundResource, Boolean multiple) {
        UserRoleManager userRoleManager = SpringContextHolder.getBean(UserRoleManager.class);
        String roleIds = passRoundResource.getResourceId();
        Object child = userRoleManager.findUserRoles(passConfig.getUserId(), roleIds, null, true, multiple, false);
        if (child != null && child instanceof JSONTreeNode) {
            return (JSONTreeNode) child;
        }
        return null;
    }
}
