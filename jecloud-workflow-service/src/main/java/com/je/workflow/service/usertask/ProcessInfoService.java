/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.model.process.model.ProcessDismissNodeInfo;
import com.je.bpm.model.process.model.ProcessGobackNodeInfo;
import com.je.bpm.model.process.model.ProcessNextNodeInfo;
import com.je.bpm.model.process.results.ProcessCirculatedElementResult;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import com.je.ibatis.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;

public interface ProcessInfoService {

    /**
     * 获取提交的节点信息和人员信息
     *
     * @param taskId 任务id
     * @param pdid   流程部署id
     * @param beanId bean信息
     * @return
     */
    List<ProcessNextNodeInfo> getSubmitOutGoingNode(String taskId, String pdid, String prod, String beanId, String tableCode);

    //    ProcessNextElementAssigneeResult getSubmitOutGoingNodeAssignee(String taskId, String pdid, String prod, String beanId, String target,String tableCode,String operationId);
    JSONObject getSubmitOutGoingNodeAssignee(String taskId, String pdid, String prod, String beanId, String target, String tableCode, String operationId);

    /**
     * 获取退回节点名称
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @return
     */
    ProcessGobackNodeInfo getGobackNode(String piid, String taskId);

    /**
     * 获取取回节点名称
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @return
     */
    ProcessGobackNodeInfo getRetrieveNode(String piid, String taskId);

    /**
     * 获取委托节点信息
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @return
     */
    ProcessNextNodeInfo getDelegationNode(String piid, String taskId);

    /**
     * 获取驳回信息
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @param pdid   流程部署id
     * @return
     */
    List<ProcessDismissNodeInfo> getDismissOutGoingNode(String piid, String taskId, String pdid);

    /**
     * 获取直送节点名称
     *
     * @param piid   流程实例id
     * @param taskId 任务id
     * @return
     */
    ProcessGobackNodeInfo getDirectDeliveryNode(String piid, String taskId);

    /**
     * 获取流程传阅人
     *
     * @return
     */
    ProcessCirculatedElementResult getPassRoundUsers(String taskId, String prod, String beanId, String tableCode) throws PayloadValidErrorException;

    /**
     * 获取可操作会签人员信息
     *
     * @param taskId 任务id
     * @return
     */
    Map<String, Object> getCounterSignerOperationalUsers(String taskId);

    String getCountersignApprovalOpinion(String taskId);

    List<Map<String, String>> getPassRoundUsersListByTaskId(String taskId,String userId);

    List<Map<String, Object>> getAssigneeByTaskId(String taskId, String currentNodeId);

    Page getUrgeData(Page page, String taskId, String piid, String currentNodeId, String type);

    JSONArray commonUserInfo();

    Map<String, Object> getReminderInformation(String beanId, String pdid, String prod, String tableCode);

}
