/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.model;

import com.je.common.base.DynaBean;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface ModelService {

    /**
     * 生成流程图
     *
     * @return
     */
    File generateImage(String beanId, String xmlId);

    List<Map<String, String>> getProcessPlanningList(String funcCode, String beanId);

    /**
     * 获取流程模型的json信息
     *
     * @param modelId 模型id
     * @return
     */
    String getJsonInfoById(String modelId);

    String getXmlInfoById(String xmlId);


    /**
     * 保存流程部署信息
     *
     * @param dynaBean     流程基础信息
     * @param metaInfoJson 流程信息json
     * @return
     */
    String doSave(DynaBean dynaBean, String metaInfoJson, String metaInfoXml);

    /**
     * 修改流程部署信息
     *
     * @param modelId      模型id
     * @param metaInfoJson 流程信息json
     * @return
     */
    String doUpdate(String modelId, String xmlId, DynaBean dynaBean, String metaInfoJson, String metaInfoXml);

    /**
     * 部署
     *
     * @param processInfoId 流程信息id
     */
    void deploy(String processInfoId);

    /**
     * 部署
     *
     * @param dynaBean 流程信息
     */
    void deploy(DynaBean dynaBean);

    /**
     * 删除模型
     *
     * @param tableCode tableCode
     * @param ids       ids
     * @return
     */
    void doRemove(String tableCode, String ids);

    /**
     * 批量取消部署
     *
     * @param tableCode
     * @param ids
     */
    void unDeploy(String tableCode, String ids);

    /**
     * 批量发布流程
     *
     * @param tableCode 表
     * @param ids       主键s
     */
    void batchDeploy(String tableCode, String ids);
}
