/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 领取
 */
@Service
public class ReceiveUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {
        completeTask(taskId);
    }

    public void completeTask(String taskId) {
        List<DynaBean> dynaBeans = metaService.select("JE_WORKFLOW_RN_TASK", ConditionsWrapper.builder().
                eq("TASK_ACTIVITI_TASK_ID", taskId));
        if (dynaBeans.size() == 0) {
            return;
        }
        DynaBean dynaBean = dynaBeans.get(0);
        //将其他人的任务办理完成
        metaService.executeSql(String.format("UPDATE JE_WORKFLOW_RN_TASK SET TASK_HANDLE='1' WHERE TASK_PIID='%s' " +
                        " and TASK_NODE_ID='%s' and ASSIGNEE_ID!='%s'",
                dynaBean.getStr("TASK_PIID"),
                dynaBean.getStr("TASK_NODE_ID"),
                Authentication.getAuthenticatedUser().getDeptId()));

        //将其他的execution的提交信息更改
        metaService.executeSql(String.format("UPDATE JE_WORKFLOW_RN_EXECUTION SET EXECUTION_SUBMIT_TYPE='RECEIVE'," +
                        "EXECUTION_LAST_COMMENT='领取任务',EXECUTION_SUBMIT_USER_NAME='%s'," +
                        "EXECUTION_SUBMIET_USER_ID='%s' WHERE EXECUTION_NODE_ID='%s' and EXECUTION_PIID='%s' and" +
                        " JE_WORKFLOW_RN_EXECUTION_ID='%s'",
                Authentication.getAuthenticatedUser().getName(),
                Authentication.getAuthenticatedUser().getDeptId(),
                dynaBean.getStr("TASK_NODE_ID"),
                dynaBean.getStr("TASK_PIID"),
                dynaBean.getStr("JE_WORKFLOW_RN_EXECUTION_ID"))
        );
    }

}
