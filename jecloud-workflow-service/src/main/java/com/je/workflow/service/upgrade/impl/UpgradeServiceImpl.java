/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upgrade.impl;

import com.google.common.base.Splitter;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.workflow.service.upgrade.UpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UpgradeServiceImpl implements UpgradeService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaResourceService metaResourceService;

    @Override
    public void insertPackageWorkflows(String upgradeId, String workflowIds) {
        List<String> workflowIdList = Splitter.on(",").splitToList(workflowIds);
        List<DynaBean> menuBeanList = metaService.select("JE_WORKFLOW_PROCESSINFO", ConditionsWrapper.builder()
                .in("JE_WORKFLOW_PROCESSINFO_ID",workflowIdList));

        deleteResourceByType(upgradeId,"workflow");
        for (DynaBean eachWorkflowBean : menuBeanList) {
            DynaBean detailBean = new DynaBean("JE_META_UPGRADERESOURCE",false);
            detailBean.set("UPGRADERESOURCE_TYPE_NAME","工作流");
            detailBean.set("UPGRADERESOURCE_TYPE_CODE","workflow");
            detailBean.set("UPGRADERESOURCE_CONTENT_NAME",eachWorkflowBean.getStr("PROCESSINFO_NAME"));
            detailBean.set("UPGRADERESOURCE_CONTENT_CODE",eachWorkflowBean.getStr("JE_WORKFLOW_PROCESSINFO_ID"));
            detailBean.set("UPGRADERESOURCE_CONTENT_BM", eachWorkflowBean.getStr("PROCESSINFO_KEY"));
            detailBean.set("JE_META_UPGRADEPACKAGE_ID",upgradeId);
            commonService.buildModelCreateInfo(detailBean);
            metaResourceService.insert(detailBean);
        }
    }

    public void deleteResourceByType(String upgradeId,String type){
        metaResourceService.deleteByTableCodeAndNativeQuery("JE_META_UPGRADERESOURCE",
                NativeQuery.build().eq("JE_META_UPGRADEPACKAGE_ID",upgradeId).eq("UPGRADERESOURCE_TYPE_CODE",type));
    }

}
