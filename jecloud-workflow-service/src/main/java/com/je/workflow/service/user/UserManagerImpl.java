/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.bpm.common.identity.ActivitiUser;
import com.je.bpm.common.identity.AllUserInfo;
import com.je.bpm.runtime.shared.identity.UserManager;
import com.je.rbac.rpc.TreatableUserRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class UserManagerImpl implements UserManager {
    @Autowired
    private TreatableUserRpcServiceImpl treatableUserRpcService;

    private static int USER_NUM = 0;

    @Override
    public ActivitiUser findUser(String userId) {
        ActivitiUser activitiUser = new ActivitiUser();
        activitiUser.setId(userId);
        int currentNum = USER_NUM + 1;
        activitiUser.setCode("user-" + currentNum);
        activitiUser.setName("name-" + currentNum);
        return activitiUser;
    }

    @Override
    public List<ActivitiUser> findUsers(Collection<String> userIds) {
        return null;
    }

    @Override
    public List<ActivitiUser> findUsersByDepartmentId(String departmentId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findUsersByRoleId(String roleId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findUsersByPositionId(String positionId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findUsersByWorkGroupId(String workGroupId, String type) {
        return null;
    }

    @Override
    public AllUserInfo findUsersByTyped(String departmentId, String roleId, String positionId, List<String> userIdList) {
        return null;
    }

    @Override
    public ActivitiUser findCurrentUser() {
        return null;
    }

    @Override
    public ActivitiUser findDirectLeader(String userId) {
        return null;
    }

    @Override
    public ActivitiUser findDeptLeader(String userId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findDeptMonitorLeader(String userId) {
        return null;
    }

    @Override
    public ActivitiUser findDeptHeader(String userId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findDeptLeaderAndMonitorLeader(String userId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findDeptLeaderAndDirectLeaderAndMonitorLeader(String userId) {
        return null;
    }

    @Override
    public ActivitiUser findStartUser(String processInstanceId) {
        return null;
    }

    @Override
    public ActivitiUser findPreTaskAssignee(String processInstance, String taskId) {
        return null;
    }

    @Override
    public ActivitiUser findPreTaskAssigneeLeader(String processInstance, String taskId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findDeptUsers(String userId, boolean isContainQueryUserId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findDeptAllUsers(String userId, boolean isContainQueryUserId) {
        return null;
    }

    @Override
    public List<ActivitiUser> findDeptAllAndMonitorDeptUsers(String userId, boolean isContainQueryUserId) {
        return null;
    }

    @Override
    public Object findUsers(List<String> list, Boolean aBoolean, Boolean multiple, Boolean addOwn) {
        Object users = treatableUserRpcService.findUsersAndBuildTreeNodeByAccountIds(list, aBoolean,multiple, addOwn);
        return users;
    }
}
