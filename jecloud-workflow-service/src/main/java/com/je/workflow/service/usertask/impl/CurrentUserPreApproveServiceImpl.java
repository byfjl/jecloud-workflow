/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask.impl;

import com.je.bpm.engine.impl.identity.Authentication;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.workflow.service.usertask.CurrentUserParam;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 待办
 */
@Service(value = "upcomingPreApproveService")
public class CurrentUserPreApproveServiceImpl extends AbstractCurrentUserService {

    @Override
    public Map<String, Object> getTask(CurrentUserParam upcomingParam) {
        ConditionsWrapper conditionsWrapper = buildConditionsWrapper(upcomingParam);
        return getRunTask(conditionsWrapper, upcomingParam);
    }

    @Override
    public Long getBadge(CurrentUserParam upcomingParam) {
        ConditionsWrapper conditionsWrapper = buildConditionsWrapper(upcomingParam);
        buildPreApproveWrapper(conditionsWrapper);
        return abstractGetRunTaskBadge("JE_WORKFLOW_V_RN_TASK", conditionsWrapper);
    }

    private Map<String, Object> getRunTask(ConditionsWrapper conditionsWrapper, CurrentUserParam upcomingParam) {
        buildPreApproveWrapper(conditionsWrapper);
        return abstractGetRunTask("JE_WORKFLOW_V_RN_TASK", conditionsWrapper, upcomingParam.getPage());
    }

    private void buildPreApproveWrapper(ConditionsWrapper conditionsWrapper) {
        conditionsWrapper.eq("TASK_DELAY", "0");
        conditionsWrapper.eq("TASK_HANDLE", "0");
        conditionsWrapper.eq("ASSIGNEE_ID", Authentication.getAuthenticatedUser().getDeptId());
    }


}
