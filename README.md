# 工作流服务项目

## 项目简介

JECloud工作流管理模块，用于统一管理平台工作流的定义、流转和监控等，核心引擎基于Activiti7.1开发，为用户提供便捷的WEB端可视化的
编辑界面。支持启动、发起、撤销、取回、提交、直送、退回、驳回、转办、委托、调拨、领取任务、改签、传阅、审阅、催办、作废、加签、
减签、回签、分支、聚合、会签、消息提醒、审批告知提醒、跳跃等多种具有中国特色的流程规则。

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- aggregate-tomcat： SpringBoot Tomcat聚合模块
- jecloud-workflow-facade: 接口门面模块
- jecloud-workflow-sdk: 消费者模块
- jecloud-workflow-service: 提供者模块

## 编译部署

使用maven生成对应环境的jar包，例: 生产环境打包
``` shell
//开发环境打包
mvn clean package -Pdev -Dmaven.test.skip=true
//本地开发环境打包
mvn clean package -Plocal -Dmaven.test.skip=true
//测试环境打包
mvn clean package -Ptest -Dmaven.test.skip=true
//生产环境打包
mvn clean package -Pprod -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)