/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.rpc;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaWorkflowService;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.ibatis.extension.toolkit.Constants;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Map;

@Service
public class MetaWorkflowRpcServiceImpl implements MetaWorkflowService {

    @RpcReference(microserviceName = "workflow",schemaId = "metaWorkflowRpcService")
    private MetaWorkflowService metaWorkflowService;

    @Override
    @Transactional
    public void clearMyBatisCache() {
        metaWorkflowService.clearMyBatisCache();
    }

    @Override
    @Transactional
    public DynaBean insertByTableCode(String tableCode, DynaBean beanMap) {
        if(Strings.isNullOrEmpty(tableCode)){
            tableCode = beanMap.getStr(Constants.KEY_TABLE_CODE);
        }
        return metaWorkflowService.insertByTableCode(tableCode,beanMap);
    }

    @Override
    @Transactional
    public List<DynaBean> insertBatchByTableCode(String tableCode, List<DynaBean> list) {
        return metaWorkflowService.insertBatchByTableCode(tableCode,list);
    }

    @Override
    @Transactional
    public DynaBean update(String tableCode, String pkValue, DynaBean beanMap, NativeQuery nativeQuery) {
        return metaWorkflowService.update(tableCode,pkValue,beanMap,nativeQuery);
    }

    @Override
    @Transactional
    public int deleteByTableCodeAndNativeQuery(String tableCode, NativeQuery nativeQuery) {
        return metaWorkflowService.deleteByTableCodeAndNativeQuery(tableCode,nativeQuery);
    }

    @Override
    @Transactional
    public List<Map<String, Object>> selectMapByPageAndNativeQuery(Page page, NativeQuery nativeQuery) {
        return metaWorkflowService.selectMapByPageAndNativeQuery(page,nativeQuery);
    }

    @Override
    @Transactional
    public List<DynaBean> selectPageWithColumns(String tableCode, Page page, NativeQuery nativeQuery, String columns) {
        return metaWorkflowService.selectPageWithColumns(tableCode,page,nativeQuery,columns);
    }

    @Override
    @Transactional
    public DynaBean selectOne(String tableCode, NativeQuery nativeQuery, String columns) {
        return metaWorkflowService.selectOne(tableCode,nativeQuery,columns);
    }

    @Override
    @Transactional
    public List<Map<String, Object>> loadByNativeQuery(String funcCode, Page page, NativeQuery nativeQuery) {
        return metaWorkflowService.loadByNativeQuery(funcCode,page,nativeQuery);
    }

    @Override
    @Transactional
    public DynaBean selectOneByPkWithColumns(String tableCode, String pkValue, String columns) {
        return metaWorkflowService.selectOneByPkWithColumns(tableCode,pkValue,columns);
    }

    @Override
    @Transactional
    public int executeSqlByNativeQuery(NativeQuery nativeQuery) {
        return metaWorkflowService.executeSqlByNativeQuery(nativeQuery);
    }

    @Override
    @Transactional
    public long countByNativeQuery(NativeQuery nativeQuery) {
        return metaWorkflowService.countByNativeQuery(nativeQuery);
    }

    @Override
    public Boolean selectDataExistsByPkValue(String tableCode, String pkValue) {
        return metaWorkflowService.selectDataExistsByPkValue(tableCode,pkValue);
    }


}
