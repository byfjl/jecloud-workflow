/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.rpc;

import com.je.common.base.workflow.service.CurrentUserTaskRpcService;
import com.je.common.base.workflow.vo.CirculationHistoryVo;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrentUserTaskRpcServiceImpl implements CurrentUserTaskRpcService {

    @RpcReference(microserviceName = "workflow", schemaId = "currentUserTaskRpcService")
    private CurrentUserTaskRpcService currentUserTaskRpcService;

    @Override
    public List<CirculationHistoryVo> getCirculationHistory(String beanId) {
        return currentUserTaskRpcService.getCirculationHistory(beanId);
    }
}
